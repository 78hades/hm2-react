import  PropTypes  from "prop-types";
import CardProduct from "./CardProduct";
 function ListProducts({
  products,
  clickCountBasket,
  setFavorites,
}) {
  const cardProduct = products.map((product) => (
    <CardProduct
      setFavorites={setFavorites}
      clickCountBasket={clickCountBasket}
      key={product.id}
      productId={product.id}
      product={product}
    ></CardProduct>
  ));
  return (
    <>
      <h3 className="products__title">Product</h3>
      <section className="products">{cardProduct}</section>
    </>
  );
}
ListProducts.propTypes = {
products: PropTypes.array.isRequired,
clickCountBasket: PropTypes.func.isRequired,
}
 export default ListProducts