import logoImg from "./icon-park-solid_game-ps.png";
import Loupe from "./Vector.png";
import Basket from "./Vector (1).png";
import Star from "./775819 (1).svg";
import PropTypes from 'prop-types'
 function Header({ productsInBasket, favorites }) {
  return (
    <>
      <header className="header">
        <div className="header__logo logo">
          <img className="logo__img" src={logoImg} alt="" />
          <div className="logo__text">
            <span className="logo__text-game">GAME</span>
            <span className="logo__text-store">STORE</span>
          </div>
        </div>
        <ul className="header__menu menu">
          <li className="menu__item">Shop</li>
          <li className="menu__item">Computer</li>
          <li className="menu__item">Game Headphones</li>
          <li className="menu__item">VR Glasses</li>
          <li className="menu__item">Keyboard</li>
          <li className="menu__item">Mouse Gaming</li>
        </ul>
        <div className="header__search">
          <img className="header__search-img" src={Loupe} alt="" />
          <input
            className="header__search-field"
            placeholder="Search"
            type="text"
          />
        </div>
        <div className="header__wishlist">
          <div className="header__wishlist-basket">
            <p>{productsInBasket}</p> <img src={Basket} alt="" />
          </div>
          <div className="header__wishlist-favorites">
            <img className="header__wishlist-favorites" src={Star} alt="" />
            <p>{favorites}</p>
          </div>
        </div>
      </header>
    </>
  );
}
Header.propTypes ={
favorites: PropTypes.number.isRequired,
productsInBasket: PropTypes.number.isRequired
}
export default Header