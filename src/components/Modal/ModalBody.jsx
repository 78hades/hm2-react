export default function ModalBody({ type, price, color }) {
  return (
    <>  <span className="products__product-type">{type}</span>
      <span className="products__product-price">{price}</span>
      <span className="products__product-color">{color}</span>
    </>
  );
}
