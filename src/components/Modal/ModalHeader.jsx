export default function ModalHeader({img, name}) {
  return <><img className="products__product-img" src={img} alt="" /> <p className="products__product-name">{name}</p></>
}
