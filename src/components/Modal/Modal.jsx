import PropTypes from 'prop-types'
import ModalWrapper from "./ModalWrapper";
import ModalHeader from "./ModalHeader";
// import ModalImage from "../ModalImage/ModalImage";
import ModalClose from "./ModalClose";
import ModalBody from "./ModalBody";
import ModalFooter from "./ModalFooter";

 function Modal({
  // stateOne,
  stateTwo,
  // onCloseFirst,
  onCloseSecond,
  product,
  clickCountBasket,
}) {
  return (
    <>
      {/* {stateOne && (
        <ModalWrapper onClick={onCloseFirst}>
          <div className="modal__content" onClick={(e) => e.stopPropagation()}>
            <ModalClose onClick={onCloseFirst}></ModalClose>
            <ModalImage></ModalImage>
            <ModalHeader>Product Delete!</ModalHeader>
            <ModalBody>
              By clicking the “Yes, Delete” button, PRODUCT NAME will be
              deleted.
            </ModalBody>

            <ModalFooter
              firstText="NO, CANCEL"
              firstClick={clickCountBasket}
              secondaryText="YES, DELETE"
              secondaryClick={() => console.log("clicked the second button")}
            ></ModalFooter>
          </div>
        </ModalWrapper>
      )} */}
      {stateTwo && (
        <div>
          <ModalWrapper onClick={onCloseSecond}>
            <div
              className="modal__content"
              onClick={(e) => e.stopPropagation()}
            >
              <ModalClose onClick={onCloseSecond}></ModalClose>
              <ModalHeader
                img={product.pathImg}
                name={product.name}
              ></ModalHeader>
              <ModalBody
                type={product.type}
                price={product.price}
                color={product.color}
              ></ModalBody>
              <ModalFooter
                firstText="ADD TO CARD"
                firstClick={clickCountBasket}
              ></ModalFooter>
            </div>
          </ModalWrapper>
        </div>
      )}
    </>
  );
}
Modal.propTypes = {
// stateOne: PropTypes.bool.isRequired,
stateTwo: PropTypes.bool.isRequired,
// onCloseFirst: PropTypes.func.isRequired,
onCloseSecond: PropTypes.func.isRequired,
}
export default Modal