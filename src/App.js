import "./App.scss";
import { useState, useEffect } from "react";
import Header from "./components/Header/Header";
import Banner from "./components/Banner/Banner";
import ListProducts from "./components/GoodsSection/ListProducts";
import Footer from "./components/Footer/Footer";
function App() {
  // const [stateModalFirst, setStateModalFirst] = useState(false);
  // const [stateModalSecond, setStateModalSecond] = useState(false);
  const [products, setProducts] = useState();
  useEffect(() => {
    async function fetchData() {
      const responceData = await fetch("/products.json");
      const data = await responceData.json();
      setProducts(data);
      return data;
    }
    fetchData();
  }, []);
  const [productsInBasket, setProductsInBasket] = useState(0);
  const [favorites, setFavorites] = useState(0);
  useEffect(() => {
    const newValueBasket = localStorage.getItem("productsInBasket");
    if (newValueBasket) {
      setProductsInBasket(JSON.parse(newValueBasket));
    }
  }, []);

  useEffect(() => {
    if (productsInBasket !== 0) {
      localStorage.setItem(
        "productsInBasket",
        JSON.stringify(productsInBasket)
      );
    }
  }, [productsInBasket]);
  useEffect(() => {
    const newValue = localStorage.getItem("favoritesCount");
    if (newValue) {
      setFavorites(JSON.parse(newValue));
    }
  }, []);
  useEffect(() => {
    if (favorites !== 0) {
      localStorage.setItem("favoritesCount", JSON.stringify(favorites));
    }
  }, [favorites]);

  const handleClickBacket = () => {
    setProductsInBasket((prevCount) => prevCount + 1);
  };

  return (
    <>
      <Header
        favorites={favorites}
        productsInBasket={productsInBasket}
      ></Header>
      <Banner></Banner>
      <ListProducts
        clickCountBasket={handleClickBacket}
        setFavorites={setFavorites}
        products={products || []}
      ></ListProducts>
      <Footer></Footer>
      {/* <Button
        type="button"
        classNames="modal__first-btn"
        onClick={() => setStateModalFirst(true)}
      >
        Open first modal
      </Button> */}
      {/* <Button
        type="button"
        classNames="modal__second-btn"
        onClick={() => setStateModalSecond(true)}
      >
        Open second modal
      </Button> */}
      {/* <Modal
      >
        {" "}
      </Modal> */}
    </>
  );
}

export default App;
